
//shared prefs

import 'dart:ui';

const String USER_ID = "USER_ID";
const String TOKEN  = "TOKEN";
const String CONNECTED  = "CONNECTED";
const String NAME  = "NAME";
const String EMAIL  = "EMAIL";
const String BASKET  = "BASKET";
const String RESTO  = "RESTO";
const String DISPO  = "DISPO";
const String PHONE  = "PHONE";


const backgroundColor = Color(0xFFf2f9fc);
const onesignalid = "13324570-0f38-4098-88f0-13c2ae46bb7a";