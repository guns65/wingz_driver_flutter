import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wingz_driver/bloc/api_settings.dart';
import 'package:wingz_driver/bloc/command_bloc/command_bloc.dart';
import 'package:wingz_driver/globals/colors.dart';
import 'package:wingz_driver/globals/constants.dart';
import 'package:wingz_driver/models/get_commands_response.dart';
import 'package:wingz_driver/screens/command_details_screen/command_details_screen.dart';
import 'package:wingz_driver/screens/menu_screen/menu_screen.dart';
import 'package:wingz_driver/services/alert_service.dart';

class CommandTabScreen extends StatefulWidget {

  final bool fromLogin;
  CommandTabScreen({this.fromLogin = false});

  @override
  State<StatefulWidget> createState() {
    return new CommandTabScreenState();
  }
}

class CommandTabScreenState extends State<CommandTabScreen> {

  final GlobalKey<ScaffoldState> _key = GlobalKey(); // Create a key
  final _commandBloc = CommandBloc();
  bool isMyCommandsSelected = true;
  bool isSwitched = false;
  var myOrdersList = List<Command>.empty();
  var ordersList = List<Command>.empty();
  bool isLoading = false;

  var data = List<Command>.empty();

  late Timer apiTimer;

  void initState() {
    super.initState();
    _getAuthStatus();
    _getStatus();
    _commandBloc.add(GetOrders());
    ///set available after login
    if(widget.fromLogin){
      _commandBloc.add(SetAvailable());
    }
  }

  _getAuthStatus() async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    bool? available = prefs.getBool(CONNECTED);

    print("connected : $available");

    if(available != null){
        apiTimer = Timer.periodic(Duration(seconds: 5), (timer) {
          print("connected : $available");
          if(available){
            if(isMyCommandsSelected){
              _commandBloc.add(GetMyOrders());
            }else{
              _commandBloc.add(GetOrders());
            }
          }
        });
    }
  }

  _getStatus() async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    bool? available = prefs.getBool(DISPO);
    if(available != null){
      setState(() {
        isSwitched = available;
      });
    }
  }

  @override
  void dispose() {
    apiTimer.cancel();
    _commandBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: Implement build
    return Scaffold(
        key: _key,
        drawer: Container(
            width: MediaQuery.of(context).size.width * 0.85,
            child: MenuScreen()
        ),
        appBar: AppBar(
          title:Text('COMMANDES', style: TextStyle(fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold)),
          centerTitle: false,
          backgroundColor: Colors.white,
          elevation: 0,
          leading:Row(
            children: [ IconButton(
              onPressed: () => _key.currentState!.openDrawer(),
              icon: Icon(Icons.menu_rounded, color: Colors.black,size: 35),
            )],
          ),
        ),
        backgroundColor: backgroundColor,
        body:  BlocListener(
      bloc: _commandBloc,
      listener: (c, CommandState state) async {
        if (state is CommandInitial) {
          print("-------state-------");
          print(state);
          isLoading = true;
        }

        if(state is SuccessMyOrdersState){
          isLoading = false;
          myOrdersList = state.response;
          data = state.response;
          isMyCommandsSelected = true;
        }

        if(state is SuccessOrdersState){
          isLoading = false;
          ordersList = state.response;
          data = state.response;
          isMyCommandsSelected = false;
        }

        if(state is UpdateDriverStatusState){
          isLoading = false;
          AlertService().showSuccessAlert(context, 'Statut modifié avec succès');
          final SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setBool(DISPO, true);
          setState(() {
            isSwitched = true;
          });
        }

        if(state is UpdateDriverUnavailableStatusState){
          isLoading = false;
          AlertService().showSuccessAlert(context, 'Statut modifié avec succès');
          final SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setBool(DISPO, false);
        }

        if (state is ErrorState ) {
          isLoading = false;
          AlertService().showErrorAlert(context, 'Erreur Serveur, veuillez réessayer plus tard.');
        }
      },
      child: BlocBuilder(
          bloc: _commandBloc,
          builder: (c, CommandState state) {
            return body(c, state);
          }),
    ));
  }

  Widget body(BuildContext context, CommandState state){
    return  Column(
      children: [

        Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
            margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(6))
            ),
            child:Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Disponibilité',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.black87,
                        fontWeight: FontWeight.bold)),
                CupertinoSwitch(
                  value: isSwitched,
                  onChanged: (value) {
                    setState(() {
                      isSwitched = value;
                      if(isSwitched){
                        _commandBloc.add(SetAvailable());
                      }else{
                        _commandBloc.add(SetUnavailable());
                      }
                    });
                  },
                  activeColor: AppColor().goldColorBg,
                ),
              ],
            )),
        SizedBox(height: 6),
        Divider(),
        SizedBox(height: 6),
        Padding(padding: EdgeInsets.symmetric(horizontal: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: GestureDetector(
                onTap: (){
                  setState(() {
                    isMyCommandsSelected = true;
                    _commandBloc.add(GetMyOrders());
                    data = myOrdersList;
                  });
                },
                child: Container(
                  height: 40,
                  decoration: BoxDecoration(
                      color: isMyCommandsSelected ? AppColor().goldColorBg : Colors.white,
                  ),
                  child: Center(
                    child: Text("Mes commandes", style: TextStyle(color: isMyCommandsSelected ? Colors.white : Colors.black87,  fontSize: 16,  fontWeight: isMyCommandsSelected ? FontWeight.bold : FontWeight.normal )),
                  ),
                ),
              ),
            ),
            Container(width: 2, color: Colors.black87, height: 40),
            Expanded(child: GestureDetector(
              onTap: (){
                  setState(() {
                    isMyCommandsSelected = false;
                    _commandBloc.add(GetOrders());
                    data = ordersList;
                  });
              },
              child: Container(
                height: 40,
                decoration: BoxDecoration(
                    color: isMyCommandsSelected ? Colors.white : AppColor().goldColorBg,
                ),
                child: Center(
                  child: Text("Commandes", style: TextStyle(color: isMyCommandsSelected ? Colors.black87 : Colors.white,   fontSize: 16,  fontWeight: isMyCommandsSelected ? FontWeight.normal : FontWeight.bold)),
                ),
              ),
            ))
          ],
        )),
        SizedBox(height: 6),
        Divider(),
        if(isLoading)
          Container(
              padding: EdgeInsets.all(8),
              child:  CircularProgressIndicator(
                color: AppColor().goldColorBg,
                strokeWidth: 5,
              )
          ),
        SizedBox(height: 6),
            Expanded(
              child:RefreshIndicator(
    onRefresh: _pullRefresh,
    child: data.length > 0 ?  ListView.builder(
                  itemCount: data.length,
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    final command = data[index];
                    return GestureDetector(
                      onTap: (){
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) => CommandDetailsScreen(command, isMyCommandsSelected))).then((value) {
                          if(value != null){
                            _commandBloc.add(GetMyOrders());
                            setState(() {
                              isMyCommandsSelected = true;
                            });
                          }
                        });

                      },child: Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                        margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(6))
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Container(
                                  height: 80,
                                  width: 80,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                          image: NetworkImage(IMAGE_URL + command.restorant.icon),
                                          fit: BoxFit.fill
                                      )
                                  ),
                                ),
                                SizedBox(width : 8),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("N° de commande : #${command.id}",style: TextStyle(fontSize: 18, color: Colors.black87, fontWeight: FontWeight.bold)),
                                    SizedBox(height: 16),
                                    Row(
                                      children: [
                                        Icon(Icons.restaurant, size: 20),
                                        SizedBox(width: 8),
                                        Container(
                                          child: Text("${command.restorant.name}", style: TextStyle(fontSize: 16, color: Colors.black87, fontWeight: FontWeight.bold)),
                                        ),                                ],
                                    ),
                                    SizedBox(height: 8),
                                    Row(
                                      children: [
                                        Icon(Icons.location_pin, size: 20),
                                        SizedBox(width: 8),
                                        Container(
                                          width: MediaQuery.of(context).size.width - 220,
                                          child: Text(command.restorant.address, style: TextStyle(fontSize: 14, color: Colors.blueGrey)),
                                        ),                                ],
                                    ),
                                    SizedBox(height: 8),
                                    Row(
                                      children: [
                                        Icon(Icons.euro, size: 20),
                                        SizedBox(width: 8),
                                        Container(
                                          child: Text("${command.orderPrice.toStringAsFixed(2)}€", style: TextStyle(fontSize: 16, color: Colors.redAccent)),
                                        ),                                ],
                                    ),
                                  ],
                                )
                              ],
                            ),
                            if(command.actions.message != "")
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(height: 8),
                                  Divider(),
                                  SizedBox(height: 8),
                                  Text("Statut",style: TextStyle(fontSize: 18, color: Colors.black87, fontWeight: FontWeight.bold)),
                                  SizedBox(height: 8),
                                  Text("${ command.actions.message }",style: TextStyle(fontSize: 14, color: Colors.black87)),
                                  SizedBox(height: 8)
                                ],
                              )
                          ],
                        )
                    ),
                    );
                  }
              ) :Center(
      child: Column(
        children: [
          SizedBox(height: 64),
          Text("Vous n'avez aucune\ncommande en cours",style: TextStyle(fontSize: 18, color: Colors.black87, fontWeight: FontWeight.bold))
        ],
      ),
    ),
              )
            )
      ],
    );
  }

  Future<void> _pullRefresh() async {
    if(isMyCommandsSelected == true){
      _commandBloc.add(GetMyOrders());
    }else{
      _commandBloc.add((GetOrders()));
    }
  }
}