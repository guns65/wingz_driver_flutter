import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wingz_driver/bloc/api_settings.dart';
import 'package:wingz_driver/bloc/command_bloc/command_bloc.dart';
import 'package:wingz_driver/globals/colors.dart';
import 'package:wingz_driver/globals/constants.dart';
import 'package:wingz_driver/models/get_commands_response.dart';
import 'package:wingz_driver/services/alert_service.dart';
import 'package:wingz_driver/services/date_service.dart';
import 'package:wingz_driver/services/ioc_container.dart';
import 'package:wingz_driver/services/location_service.dart';
import 'dart:io' show Platform;

class CommandDetailsScreen extends StatefulWidget {
  final Command order;
  final bool fromMyOrders;
  CommandDetailsScreen(this.order, this.fromMyOrders);

  @override
  State<StatefulWidget> createState() {
    return new CommandDetailsScreenState();
  }
}

class CommandDetailsScreenState extends State<CommandDetailsScreen> {

  var _dateService  = container.resolve<DateService>();
  var _locationService  = container.resolve<LocationService>();
  var _alertService  = container.resolve<AlertService>();
  final _commandBloc = CommandBloc();

  @override
  void dispose() {
    _commandBloc.close();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: Implement build
    return Scaffold(
        appBar: AppBar(
          title:Text('COMMANDES', style: TextStyle(fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold)),
          centerTitle: false,
          backgroundColor: Colors.white,
          elevation: 0,
          leading:Row(
            children: [ IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: Icon(Icons.chevron_left, color: Colors.black,size: 35),
            )],
          ),
        ),
        backgroundColor: backgroundColor,
        body:  BlocListener(
          bloc: _commandBloc,
          listener: (c, CommandState state) async {
            if (state is CommandInitial) {
              print("-------state-------");
              print(state);
              EasyLoading.show(status: 'WINGZ');
            }


            if(state is SuccessUpdateCommand){
              EasyLoading.dismiss();
              AlertService().showCommandSuccessAlert(context, 'Statut modifié avec succès.', (){
                Navigator.of(context).pop();
                Navigator.of(context).pop(true);
              });
            }

            if(state is SuccessAcceptCommandState){
              EasyLoading.dismiss();
              AlertService().showCommandSuccessAlert(context, 'Commande acceptée avec succès.', (){
                Navigator.of(context).pop();
                Navigator.of(context).pop(true);
              });
            }

            if(state is SuccessRefuseCommandState){
              EasyLoading.dismiss();
              AlertService().showCommandSuccessAlert(context, 'Commande refusée avec succès.', (){
                Navigator.of(context).pop();
                Navigator.of(context).pop(true);
              });
            }

            if (state is ErrorState ) {
              EasyLoading.dismiss();
              AlertService().showErrorAlert(context, 'Erreur Serveur, veuillez réessayer plus tard.');
            }
          },
          child: BlocBuilder(
              bloc: _commandBloc,
              builder: (c, CommandState state) {
                return body(c, state);
              }),
        ));
  }

  Widget body(BuildContext context, CommandState state){
    return SingleChildScrollView(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
               widget.fromMyOrders ? SizedBox() :  Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                  margin: EdgeInsets.symmetric(vertical: 8, horizontal: 32),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(6))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Expanded(
                            child: GestureDetector(
                              onTap: (){
                                _alertService.showAcceptCommand(context, "Êtes-vous sûr de vouloir accepter cette commande ? ", (){
                                  Navigator.of(context).pop();
                                  _commandBloc.add(AcceptCommand(widget.order.id));
                                });
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(vertical: 4),
                                decoration: BoxDecoration(
                                    color: Colors.green,
                                    borderRadius: BorderRadius.all(Radius.circular(6))),
                                child: Center(
                                  child: Icon(Icons.check_circle_outline, color: Colors.white,size: 30),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: 16),
                          Expanded(child: GestureDetector(
                            onTap: (){
                              _alertService.showCancelCommand(context, "Êtes-vous sûr de vouloir refuser cette commande ?", (){
                                Navigator.of(context).pop();
                                _commandBloc.add(RejectCommand(widget.order.id));
                              });

                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(vertical: 4),
                              decoration: BoxDecoration(
                                  color: Colors.redAccent,
                                  borderRadius: BorderRadius.all(Radius.circular(6))),
                              child: Center(
                                child: Icon(Icons.cancel_outlined, color: Colors.white,size: 30),
                              ),
                            ),
                          ))

                        ],
                      )
                    ],
                  ),
                ),


                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                  margin: EdgeInsets.symmetric(vertical: 8, horizontal: 32),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(6))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(_dateService.convertDate(widget.order.updatedAt, "yyyy MMM d - HH:mm"),style: TextStyle(fontSize: 15, color: Colors.red, fontWeight: FontWeight.bold)),
                      SizedBox(height: 8),
                      Text("${ widget.order.actions.message }",style: TextStyle(fontSize: 14, color: Colors.black87)),
                      SizedBox(height: 8),
                      if(widget.order.actions.buttons.length > 0 && widget.order.actions.buttons.first == "picked_up")
                        GestureDetector(
                            onTap: (){
                              //"picked_up":"6",
                              _commandBloc.add(UpdateStatusCommand(widget.order.id, 6));
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(vertical: 8),
                              decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.all(Radius.circular(6))),
                              child: Center(
                                child: Text("Ramassé",style: TextStyle(fontSize: 16, color: Colors.white)),
                              ),
                            ),
                          ),
                      if(widget.order.actions.buttons.length > 0 && widget.order.actions.buttons.first == "delivered")
                        GestureDetector(
                          onTap: (){
                            //"delivered":"7",
                            _commandBloc.add(UpdateStatusCommand(widget.order.id, 7));
                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(vertical: 8),
                            decoration: BoxDecoration(
                                color: Colors.green,
                                borderRadius: BorderRadius.all(Radius.circular(6))),
                            child: Center(
                              child: Text("Livré",style: TextStyle(fontSize: 16, color: Colors.white)),
                            ),
                          ),
                        ),

                    ],
                  ),
                ),


                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                  margin: EdgeInsets.symmetric(vertical: 8, horizontal: 32),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(6))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Container(
                            height: 80,
                            width: 80,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: NetworkImage(IMAGE_URL + widget.order.restorant.icon),
                                    fit: BoxFit.fill
                                )
                            ),
                          ),
                          SizedBox(width: 12),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(widget.order.restorant.name,style: TextStyle(fontSize: 18, color: Colors.black87, fontWeight: FontWeight.bold)),
                              SizedBox(height: 8),
                              Row(
                                children: [
                                  Icon(Icons.location_pin, size: 20),
                                  SizedBox(width: 8),
                                  Container(
                                    width: MediaQuery.of(context).size.width - 220,
                                    child: Text(widget.order.restorant.address, style: TextStyle(fontSize: 14, color: Colors.blueGrey)),
                                  ),
                                ],
                              ),
                              SizedBox(height: 8),
                              Row(
                                children: [
                                  Icon(Icons.phone, size: 20),
                                  SizedBox(width: 8),
                                  Text(widget.order.restorant.phone,style: TextStyle(fontSize: 16, color: Colors.black, fontWeight: FontWeight.bold)),
                                ],
                              ),
                            ],
                          )
                        ],
                      ),
                      SizedBox(height: 6),
                      Divider(),
                      SizedBox(height: 6),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Expanded(
                            child: GestureDetector(
                              onTap: (){
                                _openUrl('tel:${widget.order.restorant.phone}');
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(vertical: 4),
                                decoration: BoxDecoration(
                                    color: Colors.green,
                                    borderRadius: BorderRadius.all(Radius.circular(6))),
                                child: Center(
                                  child: Icon(Icons.phone, color: Colors.white,size: 30),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: 16),
                          Expanded(child: GestureDetector(
                            onTap: (){

                              _openMapUrl(double.parse(widget.order.restorant.lat), double.parse(widget.order.restorant.lng));

                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(vertical: 4),
                              decoration: BoxDecoration(
                                  color: AppColor().goldColorBg,
                                  borderRadius: BorderRadius.all(Radius.circular(6))),
                              child: Center(
                                child: Icon(Icons.map_outlined, color: Colors.white,size: 30),
                              ),
                            ),
                          ))

                        ],
                      )
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                  margin: EdgeInsets.symmetric(vertical: 8, horizontal: 32),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(6))),
                  child: Column(
                    children: [
                      for(final artice in widget.order.items)
                        Container(
                          padding: EdgeInsets.symmetric( vertical: 4),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [

                              Row(
                                children: [
                                  Container(
                                    height: 40,
                                    width: 40,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                            image: NetworkImage(IMAGE_URL + artice.icon),
                                            fit: BoxFit.fill
                                        )
                                    ),
                                  ),
                                  SizedBox(width: 16),
                                  Container(
                                    width: MediaQuery.of(context).size.width - 180,
                                    child: Text(artice.name.toLowerCase().capitalize(),style: TextStyle(fontSize: 18, color: Colors.black87)),
                                  ),

                                ],
                              ),

                              Text("x${artice.pivot.qty}",style: TextStyle(fontSize: 16, color: Colors.black87)),
                            ],
                          ),
                        )
                    ],
                  ),
                ),

                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                  margin: EdgeInsets.symmetric(vertical: 8, horizontal: 32),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(6))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Adresse de livraison', style: TextStyle(fontSize: 18, color: Colors.black87, fontWeight: FontWeight.bold)),
                      SizedBox(height: 12),
                      Row(
                        children: [
                          Icon(Icons.home, size: 30, color: AppColor().goldColorBg),
                          SizedBox(width: 8),
                          Container(
                            width: MediaQuery.of(context).size.width - 140,
                            child: Text(widget.order.address.address, style: TextStyle(fontSize: 16, color: Colors.blueGrey)),
                          ),
                        ],
                      ),
                      SizedBox(height: 6),
                      Divider(),
                      SizedBox(height: 6),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Expanded(
                            child: GestureDetector(
                              onTap: (){
                                _openUrl('tel:${widget.order.client.phone}');
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(vertical: 4),
                                decoration: BoxDecoration(
                                    color: Colors.green,
                                    borderRadius: BorderRadius.all(Radius.circular(6))),
                                child: Center(
                                  child: Icon(Icons.phone, color: Colors.white,size: 30),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: 16),
                          Expanded(child: GestureDetector(
                            onTap: (){
                                _openMapUrl(double.parse(widget.order.address.lat), double.parse(widget.order.address.lng));
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(vertical: 4),
                              decoration: BoxDecoration(
                                  color: AppColor().goldColorBg,
                                  borderRadius: BorderRadius.all(Radius.circular(6))),
                              child: Center(
                                child: Icon(Icons.map_outlined, color: Colors.white,size: 30),
                              ),
                            ),
                          ))

                        ],
                      )
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                  margin: EdgeInsets.symmetric(vertical: 8, horizontal: 32),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(6))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Détails', style: TextStyle(fontSize: 18, color: Colors.black87, fontWeight: FontWeight.bold)),
                      SizedBox(height: 12),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Commande",style: TextStyle(fontSize: 16, color: Colors.black87)),
                          Text("${widget.order.orderPrice.toStringAsFixed(2)}€",style: TextStyle(fontSize: 16, color: Colors.black54)),
                        ],
                      ),
                      SizedBox(height: 8),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Frais de livraison",style: TextStyle(fontSize: 16, color: Colors.black87)),
                          Text("${widget.order.deliveryPrice.toStringAsFixed(2)}€",style: TextStyle(fontSize: 16, color: Colors.black54)),
                        ],
                      ),
                      SizedBox(height: 8),
                      Divider(),
                      SizedBox(height: 8),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Total",style: TextStyle(fontSize: 16, color: Colors.black87, fontWeight: FontWeight.bold)),
                          Text("${(widget.order.deliveryPrice + widget.order.orderPrice).toStringAsFixed(2)}€",style: TextStyle(fontSize: 18, color: AppColor().goldColorBg, fontWeight: FontWeight.bold)),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 32),

              ]
          )
    );
  }

  String generateStatut(String statut){
    if (statut == 'Créé' || statut == "Accepté par l'admin" || statut == 'Accepté par le restaurant' || statut == 'A livrer' || statut == 'Rejeté par livreur' || statut == 'Accepté par livreur'){
      return "En cours";
    }else if (statut == "Rejeté par l'admin" || statut == 'Rejeté par le restaurant'){
      return "Refusé";
    }else{
      return statut;
    }
  }

  Future<void> _openUrl(String url) async {
    print(url);
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      AlertService().showErrorAlert(context, "Une erreur c'est produite, veuillez raissayer plus tard.");

      throw 'Could not launch $url';
    }
  }

  _openMapUrl(double lat, double lng){
    if (Platform.isAndroid) {
      // Android-specific code
      final url = 'http://maps.google.com/maps?daddr=$lat,$lng' ;
      _openUrl(url);

    } else if (Platform.isIOS) {
      // iOS-specific code
      final url = 'http://maps.apple.com/maps?daddr=$lat,$lng' ;
      _openUrl(url);
    }
  }

}

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}
