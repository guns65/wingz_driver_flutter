import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:wingz_driver/bloc/api_settings.dart';
import 'package:wingz_driver/bloc/command_bloc/command_bloc.dart';
import 'package:wingz_driver/globals/constants.dart';
import 'package:wingz_driver/models/get_notifications_model.dart';
import 'package:wingz_driver/screens/menu_screen/menu_screen.dart';
import 'package:wingz_driver/services/alert_service.dart';
import 'package:wingz_driver/services/date_service.dart';
import 'package:wingz_driver/services/ioc_container.dart';

class NotificationsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new NotificationsScreenState();
  }
}

class NotificationsScreenState extends State<NotificationsScreen> {

  final GlobalKey<ScaffoldState> _key = GlobalKey(); // Create a key
  final _commandBloc = CommandBloc();
  bool isLoading = false;
  var data = List<NotificationUser>.empty();
  var _dateService  = container.resolve<DateService>();

  @override
  void initState() {
    super.initState();
    _commandBloc.add(GetNotifications());
  }

  @override
  void dispose() {
    _commandBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: Implement build
    return Scaffold(
        key: _key,
        drawer: Container(
            width: MediaQuery.of(context).size.width * 0.85,
            child: MenuScreen()
        ),
        appBar: AppBar(
          title:Text('NOTIFICATIONS', style: TextStyle(fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold)),
          centerTitle: false,
          backgroundColor: Colors.white,
          elevation: 0,
          leading:Row(
            children: [ IconButton(
              onPressed: () => _key.currentState!.openDrawer(),
              icon: Icon(Icons.menu_rounded, color: Colors.black,size: 35),
            )],
          ),
        ),
        backgroundColor: backgroundColor,

        body:  BlocListener(
      bloc: _commandBloc,
      listener: (c, CommandState state) async {
        if (state is CommandInitial) {
          print("-------state-------");
          print(state);
          EasyLoading.show(status: 'WINGZ');
        }

        if(state is SuccessNotificationsState){
          EasyLoading.dismiss();
          data = state.response;
        }

        if (state is ErrorState ) {
          EasyLoading.dismiss();
          AlertService().showErrorAlert(context, 'Erreur Serveur, veuillez réessayer plus tard.');
        }
      },
      child: BlocBuilder(
          bloc: _commandBloc,
          builder: (c, CommandState state) {
            return body(c, state);
          }),
    ));
  }

  Widget body(BuildContext context, CommandState state){

    if(data.length == 0){
      return Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Center(child: Text("Vous n'avez aucune notification", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold))
          ))
      ;
    }

    return  ListView.builder(
        itemCount: data.length,
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          final notif = data[index];
          return GestureDetector(
            onTap: (){

            },child: Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
              margin: EdgeInsets.symmetric(vertical: 8, horizontal: 32),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(6))
              ),
              child : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 8),
                  Text(notif.data.title, style: TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.bold)),
                  SizedBox(height: 8),
                  Text(notif.data.body,style: TextStyle(fontSize: 16, color: Colors.blueGrey)),
                  SizedBox(height: 12),
                  Text(_dateService.convertDate(notif.createdAt, "MMM d, yyyy  HH:mm"),style: TextStyle(fontSize: 14, color: Colors.grey))
                ],
              )

          ),
          );
        });
  }
}