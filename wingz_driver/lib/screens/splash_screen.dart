import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wingz_driver/globals/constants.dart';
import 'package:wingz_driver/screens/commands_screen/commands_screen.dart';

import 'login_screen/login_screen.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {


  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 3), () {
      // Do something
      _checkConnexionState();
    });

  }

  _checkConnexionState() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var connected = prefs.getBool(CONNECTED);

    if(connected != null){
    if(connected){
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => CommandTabScreen()));
    }else{
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => LoginScreen()));
      }
    }else{
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => LoginScreen()));
    }

  }

  @override
  Widget build(BuildContext context) {
    // TODO: Implement build
    return Scaffold(
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              color: Colors.black
          ),
          child: Center(
            child: Image.asset("assets/images/logo.png", width:  MediaQuery.of(context).size.width * 0.6),
          ),
        )
    );
  }
}