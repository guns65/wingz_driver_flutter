import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wingz_driver/bloc/auth_bloc/auth_bloc.dart';
import 'package:wingz_driver/globals/colors.dart';
import 'package:wingz_driver/globals/constants.dart';
import 'package:wingz_driver/globals/decoration_tf.dart';
import 'package:wingz_driver/globals/mixins.dart';
import 'package:wingz_driver/screens/login_screen/login_screen.dart';
import 'package:wingz_driver/screens/menu_screen/menu_screen.dart';
import 'package:wingz_driver/services/alert_service.dart';

class ProfileTabScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new ProfileTabScreenState();
  }
}

class ProfileTabScreenState extends State<ProfileTabScreen> with ValidationMixin {
  final GlobalKey<ScaffoldState> _key = GlobalKey(); // Create a key

  var email = "";
  var name  = "";
  var connected = false;

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  final _authBloc = AuthBloc();

  @override
  void initState() {
    super.initState();
    _getUserData();
  }

  @override
  void dispose() {
    _authBloc.close();
    super.dispose();
  }

  _getUserData() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      nameController.text = prefs.getString(NAME) ?? "-";
      emailController.text = prefs.getString(EMAIL) ?? "-";
      phoneController.text = prefs.getString(PHONE) ?? "-";
      connected = prefs.getBool(CONNECTED) ?? false;
    });
  }

  _clearSharedPreferences() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _key,
        drawer: Container(
            width: MediaQuery.of(context).size.width * 0.85,
            child: MenuScreen()
        ),
        appBar: AppBar(
          title:Text('PROFIL', style: TextStyle(fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold)),
          centerTitle: false,
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading:Row(
            children: [ IconButton(
              onPressed: () => _key.currentState!.openDrawer(),
              icon: Icon(Icons.menu_rounded, color: Colors.black,size: 35),
            )],
          ),
        ),
        body:  BlocListener(
          bloc: _authBloc,
          listener: (c, AuthState state) async {
            if (state is AuthInitial) {
              print("-------state-------");
              print(state);
              EasyLoading.show(status: 'WINGZ');
            }

            if (state is SuccessUpdateState) {
              print("Auth success ${state.response.data.email} ${state.response.data.name} ${state.response.data.phone}");
              EasyLoading.dismiss();
              final SharedPreferences prefs = await SharedPreferences.getInstance();
              prefs.setString(NAME, state.response.data.name);
              prefs.setString(EMAIL, state.response.data.email );
              prefs.setString(PHONE, state.response.data.phone );
              AlertService().showSuccessAlert(context, 'Opération effectuée avec succès');

            }

            if (state is SuccessUpdatePasswordState) {
              EasyLoading.dismiss();
              AlertService().showSuccessAlert(context, 'Opération effectuée avec succès');
            }


            if (state is ErrorState ) {
              EasyLoading.dismiss();
              AlertService().showErrorAlert(context, 'Veuillez vérifier vos coordonnées');
            }
          },
          child: BlocBuilder(
              bloc: _authBloc,
              builder: (c, AuthState state) {
                return body(c, state);
              }),
        )
    );
  }

  Widget body(BuildContext context, AuthState state){
    // TODO: Implement build
    return SingleChildScrollView(
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/profile_bg.png"),
                    fit: BoxFit.fill
                ),
              ),
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 48),
                margin: EdgeInsets.symmetric(vertical: 96, horizontal: 32),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(6))
                ),
                child: Column(
                  children: [
                    SizedBox(height: 60),
                    if(connected) nameField(),
                    if(connected) SizedBox(height: 16),
                    if(connected) phoneField(),
                    if(connected) SizedBox(height: 16),
                    if(connected) emailField(),
                    if(connected) SizedBox(height: 16),
                    if(connected) Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                       // updateBtn(),
                        logoutBtn()
                      ],
                    ),
                    Divider(color: Colors.grey),
                    SizedBox(height: 8),
                    if(connected)  GestureDetector(
                      onTap: (){

                        Alert(
                            context: context,
                            title: "Modifier le mot de passe",
                            content: Column(
                              children: <Widget>[
                                SizedBox(height: 8),
                                emailField(),
                                SizedBox(height: 8),
                                passwordField()
                              ],
                            ),
                            buttons: [
                              DialogButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                  var data = {
                                    "email": emailController.text,
                                    "password": passwordController.text,
                                  };

                                  _authBloc.add(UpdateUserPassword(data));
                                },
                                child: Text(
                                  "ENVOYER",
                                  style: TextStyle(color: Colors.white, fontSize: 20),
                                ),
                              )
                            ]).show();

                      },
                      child: Container(
                        height: 32,
                        child: Center(
                          child:Text('Modifier le mot de passe',style: TextStyle(fontSize: 16, color: Colors.blueGrey)),
                        ),
                      ),
                    ),
                    Divider(color: Colors.grey),
                    if (connected) SizedBox(height: 8),
                    GestureDetector(
                        onTap: () {
                          _openUrl("https://wingz.delivery/public/pages/2");
                        },
                        child:  Container(
                            height: 32,
                            color: Colors.transparent,
                            width: MediaQuery.of(context).size.width,
                            child:  Center(
                              child: Text("Conditions générales d'utilisation",
                                  style: TextStyle(fontSize: 16, color: Colors.blueGrey)),
                            )
                        )
                    ),
                    SizedBox(height: 8),
                    Divider(color: Colors.grey),
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 12),
              child: Image.network('https://i0.wp.com/cdn1.iconfinder.com/data/icons/flat-business-icons/128/user-512.png?ssl=1', height: 168)
              ,
            )
          ],
        )
    );
  }

  Future<void> _openUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget logoutBtn() {
    return Container(
        margin: EdgeInsets.only(top: 8, bottom: 16),
        height: 38,
        width: 120,
        decoration: BoxDecoration(
            color: Colors.redAccent,
            borderRadius: const BorderRadius.all(Radius.circular(6))),
        child: TextButton(
            onPressed: () {
              _clearSharedPreferences();
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => LoginScreen()));
            },
            child: Text('Deconnexion',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 14.0,
                    fontWeight: FontWeight.w600))));
  }

  Widget updateBtn() {
    return Container(
        margin: EdgeInsets.only(top: 8, bottom: 16),
        height: 38,
        width: 120,
        decoration: BoxDecoration(
            color: AppColor().goldColorBg,
            borderRadius: const BorderRadius.all(Radius.circular(6))),
        child: TextButton(
            onPressed: () {
              var data = {
                "name": nameController.text,
                "email": emailController.text,
                "phone": phoneController.text,
                "app_secret": "app_secret",
              };

              _authBloc.add(UpdateUser(data));

            },
            child: Text('Modifier',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 14.0,
                    fontWeight: FontWeight.w600))));
  }

  Widget nameField() {
    return Container(
        child: TextFormField(
          cursorColor: Colors.black,
          controller: nameController,
          enabled: false,
          style: TextStyle(
              fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black, letterSpacing: 2),
          decoration: DecorationTF().decorationCalTFWithPlaceholder('Nom', Colors.white.withAlpha(150), Icon(Icons.person_rounded, color: Colors.black,)),
          validator: validateNotEmpty,
        ));
  }

  Widget phoneField() {
    return Container(
        child: TextFormField(
          cursorColor: Colors.black,
          keyboardType: TextInputType.phone,
          controller: phoneController,
          enabled: false,
          style: TextStyle(
              fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black, letterSpacing: 2),
          decoration: DecorationTF().decorationCalTFWithPlaceholder('Téléphone', Colors.white.withAlpha(150), Icon(Icons.phone, color: Colors.black,)),
          validator: validateNotEmpty,
        ));
  }

  Widget emailField() {
    return Container(
        child: TextFormField(
          cursorColor: Colors.black,
          controller: emailController,
          keyboardType: TextInputType.emailAddress,
          enabled: false,
          style: TextStyle(
              fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
          validator: validateEmail,
          decoration: DecorationTF().decorationCalTFWithPlaceholder('Email', Colors.white.withAlpha(150), Icon(Icons.email, color: Colors.black)),
        ));
  }

  Widget passwordField() {
    return Container(
        child: TextFormField(
          controller: passwordController,
          cursorColor: Colors.black,
          obscureText: true,
          style: TextStyle(
              fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black, letterSpacing: 2),
          decoration: DecorationTF().decorationCalTFWithPlaceholder('Mot de passe', Colors.white.withAlpha(150), Icon(Icons.lock_rounded, color: Colors.black,)),
          validator: validateNotEmpty,
        ));
  }

}