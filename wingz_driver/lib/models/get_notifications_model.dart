class NotificationModel {

  NotificationModel({
    required this.data,
  });
  late final List<NotificationUser> data;

  NotificationModel.fromJson(Map<String, dynamic> json){
    data = List.from(json['data']).map((e)=>NotificationUser.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['data'] = data.map((e)=>e.toJson()).toList();
    return _data;
  }
}

class NotificationUser {
  NotificationUser({
    required this.id,
    required this.type,
    required this.notifiableType,
    required this.notifiableId,
    required this.data,
    this.readAt,
    required this.createdAt,
    required this.updatedAt,
  });
  late final String id;
  late final String type;
  late final String notifiableType;
  late final int notifiableId;
  late final NotificationUserData data;
  late final Null readAt;
  late final String createdAt;
  late final String updatedAt;

  NotificationUser.fromJson(Map<String, dynamic> json){
    id = json['id'];
    type = json['type'];
    notifiableType = json['notifiable_type'];
    notifiableId = json['notifiable_id'];
    data = NotificationUserData.fromJson(json['data']);
    readAt = json['read_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['type'] = type;
    _data['notifiable_type'] = notifiableType;
    _data['notifiable_id'] = notifiableId;
    _data['data'] = data.toJson();
    _data['read_at'] = readAt;
    _data['created_at'] = createdAt;
    _data['updated_at'] = updatedAt;
    return _data;
  }
}

class NotificationUserData {
  NotificationUserData({
    required this.body,
    required this.title
  });

  late final String title;
  late final String body;


  NotificationUserData.fromJson(Map<String, dynamic> json){
    title = json['title'];
    body = json['body'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['title'] = title;
    _data['body'] = body;
    return _data;
  }
}