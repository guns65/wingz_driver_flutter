import 'package:intl/intl.dart';

class DateService {

  String convertDate(String date, String format){

    DateFormat dateFormat = DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    DateTime dateTime = dateFormat.parse(date);

    DateFormat dateStrFormat = DateFormat(format);

    String string = dateStrFormat.format(dateTime);

    return string;
  }


}

