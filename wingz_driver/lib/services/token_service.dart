
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wingz_driver/globals/constants.dart';

class TokenService {

  Future<String> getAccessToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final authInfo = prefs.getString(TOKEN);
    return "$authInfo";
  }

}
