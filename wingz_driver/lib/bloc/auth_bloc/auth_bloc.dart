import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:wingz_driver/models/login_response.dart';
import 'package:wingz_driver/models/profile_response_model.dart';
import 'package:wingz_driver/models/register_response.dart';
import 'package:wingz_driver/services/http_service.dart';
import 'package:wingz_driver/services/ioc_container.dart';

import '../api_settings.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc() : super(AuthInitial());

  var _dio = Dio();
  var _httpService  = container.resolve<HttpService>();


  @override
  Stream<AuthState> mapEventToState(
      AuthEvent event,
      ) async* {
    // TODO: implement mapEventToState
    if (event is LoginUser) {
      try {
        print("Calling Login user");
        yield AuthInitial();
        var url = BASE_URL + LOGIN_URL;

        //one signal
        var playerId = '';
        var deviceState = await OneSignal.shared.getDeviceState();
        if (deviceState != null || deviceState!.userId != null){
          playerId = deviceState.userId!;
        }

        Map data = {"email": event.email,
          "password": event.password,
          "one_signal_id" :  playerId};

        print(data);

        final Response<dynamic> response = await _dio.post(url, data: data).catchError((error) => null);
        print(response);

        if (response.statusCode == 200) {
          LoginResponse authData = LoginResponse.fromJson(response.data);
          yield LoginUserState(authData);
        } else {
          yield ErrorState('error');
        }
      } catch (e) {
        yield ErrorState("error login user");
      }
    }else  if (event is RegisterUser) {
      try {
        print("Calling register user");
        yield AuthInitial();
        var url = BASE_URL + REGISTER_URL;

        final Response<dynamic> response = await _dio.post(url, data: event.params).catchError((error) => null);

        print(response);

        if (response.statusCode == 200) {
          RegisterResponse authData = RegisterResponse.fromJson(response.data);
          yield RegisterUserState(authData);

        } else {
          yield ErrorState('error');
        }
      } catch (e) {
        yield ErrorState("error register user");
      }
    }else  if (event is UpdateUser) {
      try {
        print("Calling update user");
        yield AuthInitial();
        var url = BASE_URL + UPDATE_URL;

        final Response<dynamic> response = await _dio.post(url, data: event.params).catchError((error) => null);
        print(response);

        if (response.statusCode == 200) {
          ProfileModel authData = ProfileModel.fromJson(response.data);
          yield SuccessUpdateState(authData);
        } else {
          yield ErrorState('error');
        }
      } catch (e) {
        yield ErrorState("error register user");
      }
    }else  if (event is UpdateUserPassword) {
      try {
        print("Calling update user");
        yield AuthInitial();
        var url = BASE_URL + UPDATE_PASSWORD_URL;

        final Response<dynamic> response = await _httpService.doPost(url, event.params);
        print(response);

        if (response.statusCode == 200) {
          yield SuccessUpdatePasswordState(true);
        } else {
          yield ErrorState('error');
        }
      } catch (e) {
        yield ErrorState("error register user");
      }
    }
  }
}
