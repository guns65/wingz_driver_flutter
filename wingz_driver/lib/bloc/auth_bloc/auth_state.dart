part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  const AuthState();
}

class AuthInitial extends AuthState {
  @override
  List<Object> get props => [];
}

class LoginUserState extends AuthState {
  final LoginResponse response;

  LoginUserState(this.response);
  @override
  List<Object> get props => [response];
}

class RegisterUserState extends AuthState {
  final RegisterResponse response;

  RegisterUserState(this.response);
  @override
  List<Object> get props => [response];
}

class ErrorState extends AuthState {
  final String error;

  ErrorState(this.error);
  @override
  List<Object> get props => [error];
}

class SuccessUpdateState extends AuthState {
  final ProfileModel response;

  SuccessUpdateState(this.response);
  @override
  List<Object> get props => [response];
}

class SuccessUpdatePasswordState extends AuthState {
  final bool response;

  SuccessUpdatePasswordState(this.response);
  @override
  List<Object> get props => [response];
}


class AddAddressState extends AuthState {
  final bool response;

  AddAddressState(this.response);
  @override
  List<Object> get props => [response];
}

